import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {
  // English
  'en': {
    // Buttons
    btnBack: 'Back',
    btnSend: 'Send',
    btnContinue: 'Continue',

    // Page Headers
    headAccount: 'Account Select',
    headProduct: 'Product Select',
    headNotes: 'Add Notes',

    // Input Labels
    lblSearchAccounts: 'Search Accounts',
    lblSearchProducts: 'Search Products',
    lblMainProductCategory: 'Main Product Category',
    lblAllCategories: 'All Categories',
    lblProductNotes: 'Product Notes',
    lblLanguage: 'Language',

    // Status Buttons
    stsInUse: 'In Use',
    stsMissing: 'Missing',
    stsDiscarded: 'Discarded',

    // List Labels
    lstAsset: 'asset',
    lstAssets: 'assets',
    lstAddress: 'Address',
    lstAccountID: 'Address ID',
    lstCategory: 'Category',
    lstInstallDate: 'Install Date',
    lstModel: 'Model',
    lstNotes: 'Notes',
    lstParentName: 'Parent Account',
    lstProductID: 'Product ID',
    lstSerial: 'Serial',
    lstShippingDate: 'Shipping / Received Date',

    // List Titles
    ttlAccount: 'Account',
    ttlProduct: 'Products',
    ttlLocation: 'Location',
    ttlStatus: 'Status',

    // Email
    emlBy: 'Created By',
    emlDate: 'Created Date',

    // Form State
    steSending: 'Sending',
    steSuccessTitle: 'Success',
    steSuccess: 'Submission Successful',
    steError1: 'Error during submission',
    steError2: 'Please try again. If the error continues contact your manager.'
  },

  // German
  'de': {
    // Buttons
    btnBack: 'Zurück',
    btnSend: 'Senden',
    btnContinue: 'Weiter',

    // Page Headers
    headAccount: 'Account auswählen',
    headProduct: 'Produkt auswählen',
    headNotes: 'Bemerkungen',

    // Input Labels
    lblSearchAccounts: 'Suche Accounts',
    lblSearchProducts: 'Suche Produkte',
    lblMainProductCategory: 'Haupt Produktkategorie',
    lblAllCategories: 'Alle Kategorien',
    lblProductNotes: 'Produkt Bemerkungen',
    lblLanguage: 'Sprache',

    // Status Buttons
    stsInUse: 'In Benutzung',
    stsMissing: 'Verschwunden',
    stsDiscarded: 'Ausrangiert',

    // List Labels
    lstAsset: 'Vermögenswert',
    lstAssets: 'Vermögenswert',
    lstAddress: 'Adresse',
    lstAccountID: 'Adresse ID',
    lstCategory: 'Kategorie',
    lstInstallDate: 'Installationsdatum',
    lstModel: 'Modell',
    lstNotes: 'Bemerkungen',
    lstParentName: 'Übergeordneter Accountname',
    lstProductID: 'Produkt ID',
    lstSerial: 'Seriennummer',
    lstShippingDate: 'Versand/Empfangsdatum',

    // List Titles
    ttlAccount: 'Account',
    ttlProduct: 'Produkt',
    ttlLocation: 'Standort',
    ttlStatus: 'Status',

    // Email
    emlBy: 'Erstellt von',
    emlDate: 'Erstelldatum',

    // Form State
    steSending: 'senden…',
    steSuccessTitle: 'Erfolg',
    steSuccess: 'Erfolgreich eingereicht',
    steError1: 'Fehler beim Einreichen',
    steError2: 'Bitte versuchen Sie es erneut. Falls der Fehler wieder auftritt, wenden Sie sich bitte an Ihren Vorgesetzten'
  }
}

const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'de', // set fallback locale
  messages, // set locale messages
});

export default i18n