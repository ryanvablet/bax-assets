import LoadingComponent from './loading.vue'
import AlertComponent from './alert.vue'

function dialogs(Vue, options={}) {
  var AlertConstructor = Vue.extend(AlertComponent);
  var LoadingConstructor = Vue.extend(LoadingComponent);
  var dialogInstance = null;
  var _deferred = null;

  Object.defineProperty(Vue.prototype, '$loading', {

    get: function () {

        return (config) => {
            if (dialogInstance) return;

            _deferred = new Promise(function (resolve, reject) {
                dialogInstance = new LoadingConstructor({
                    el: document.createElement('div'),
                    data() {
                        return {
                            message: config.message,
                            promise: config.promise
                        }
                    },
                    created () {
                        this.promise().then((val) => {
                            console.log(val)
                            dialogInstance.loaded()
                        })
                    },
                    methods: {
                        loaded (){
                            dialogInstance.$el.remove();
                            dialogInstance = null;
                            resolve({data: 'loaded'})
                        }
                    }
                });

                document.body.appendChild(dialogInstance.$el);
            });

            return _deferred
        };
    }

});

    Object.defineProperty(Vue.prototype, '$alert', {

        get: function () {

            return (config) => {
                if (dialogInstance) return;

                _deferred = new Promise(function (resolve, reject) {
                    dialogInstance = new AlertConstructor({
                        el: document.createElement('div'),
                        data() {
                            return {
                                title: config.title,
                                message: config.message,
                                confirmTxt: config.confirmTxt || options.confirmTxt || 'OK'
                            };
                        },
                        methods: {
                            confirm (){
                                dialogInstance.$el.remove();
                                dialogInstance = null;
                                resolve({data: 'confirm'})
                            }
                        }
                    });

                    document.body.appendChild(dialogInstance.$el);
                });

                return _deferred
            };
        }

    });

}

export default dialogs