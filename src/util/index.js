import _ from 'lodash'
import Translations from '@/data/Translations'

import axios from 'axios'

// The SOQL Query
const SOQLQuery = "SELECT CMA_Account__c, CMA_Account__r.Name, CMA_Account__r.BillingCity, CMA_Account__r.BillingState, CMA_Account__r.BST_COUNTRY__c, SerialNumber, BST_Model_ID__c, InstallDate, BST_Shipped_Received_Date__c, Product2.Name, BST_Main_Product_Category__c, Product2Id, BST_Location__c, BST_Ownership__c, BST_Business_Purpose__c, Status FROM Asset WHERE BST_Ownership__c = 'B' AND BST_Business_Purpose__c != 'SO Sold Unit' AND Status = 'Active' AND CMA_Account__c <> ''"

export function formatData (sfData) {
  console.log('formatData called')
  let formattedData = _.reduce(sfData, function(result, value, key) {
    // If no CMA_Account__r then back out
    if (value.CMA_Account__r === null) {
      console.log('Error Finding CMA_Account__r')
      result['1'] = {
        Account: {
          Id: '1',
          Name: 'ERROR',
          City: 'ERROR',
          State: 'ERROR',
          Country: 'ERROR'
        },
        Assets: []
      }
      //result['1'].Assets.push(value)
      return result
    }

    // Create object template by CMA_Account__c (Customer Account) if it doesn't exist
    // Use the Customer Account Relation (CMA_Account__r) to access the Customer Account properties
    (result[value.CMA_Account__c] || (result[value.CMA_Account__c] = {
      Account: {
        Id: value.CMA_Account__c,
        Name: value.CMA_Account__r['Name'],
        ParentName: value.CMA_Account__r['BST_PARENT_ACCOUNT_NAME__c'] ? value.CMA_Account__r['BST_PARENT_ACCOUNT_NAME__c'] : '',
        City: value.CMA_Account__r['BillingCity'],
        State: value.CMA_Account__r['BillingState'],
        Country: value.CMA_Account__r['BST_COUNTRY__c'] ? value.CMA_Account__r['BST_COUNTRY__c'] : ''
      },
      Assets: []
    }))

    // Push each asset to assets
    const asset = {
      ...value,
      status: 'Active', // In Use is default status
      notes: '' // Notes field
    }
    result[value.CMA_Account__c].Assets.push(asset)
    
    return result
  }, {})
  return formattedData
}

// Make the Vablet Call and register the event handler
export const performSalesforceQuery = (query=SOQLQuery) => {
  return new Promise((res, rej) => {
    try {
      VabletNativeInterface.callNativeMethod('performSalesforceQuery', {
        "SOQLQuery": query
      }, function(response) {
        res({success: true, data: response})
      })
    } catch(e) {
      rej(e)
    }
  })
}

// register the salesforce query handler
// data is salesforce data object passed from Vue root state
export const registerSalesForceQueryHandler = (data) => {
  // The salesforce call back handler
  // Nested to have access to passed data.
  const soqlQueryHandler = (response) => {
    if (!response.success) { return }

    alert(JSON.stringify(response))

    // Current Records, Expected Total Size, Has More Records to be returned
    const records = response.salesForceReturn.records
    const totalSize = response.salesForceReturn.totalSize
    const hasMoreRecords = response.salesForceReturn.hasMoreRecords

    // Merge the data set
    data.totalSize = totalSize
    data.hasMoreRecords = hasMoreRecords
    data.records.concat(records)

    alert('Data Update records ' + data.records.length + ' / ' + data.totalSize)
  }

  return new Promise((res, rej) => {
    // Try to register the handler
    try {
      VabletNativeInterface.registerHandler("SOQLQueryHandler",soqlQueryHandler)
      res({success:true, data})
    } catch (e) {
      res({success:false, data, error: e})
    }
  }) 
}

// Format data from SalesForce for App Usage
export const getDataFromSF = async () => {
  let sfData = []
  
  // Get SalesForce Data from the performSalesforceQuery promise.
  try {
    // The Query to get Contacts      
    const query = SOQLQuery
    sfData = await performSalesforceQuery(query)
  } catch (error) {
    const resJson = await axios.get('BaxterAssetsData.json')
    console.log((resJson.data.records))
    sfData = resJson.data.records
    alert('Loading Debug Data')
  }

  return formatData(sfData)
}

export const getDeviceUser = () => new Promise(
  (resolve, reject) => {
    let user = {name: '', email: ''}
    try {
      VabletNativeInterface.callNativeMethod('GetInfoManifest', {}, function responseCallback(responseData) {
        user.email = responseData.infoManifest.Email;
        user.name = responseData.infoManifest.DeviceName;
        resolve(user)
      });
    } catch (e) {
      console.log('Error getting email: ' + e);
      user.email = 'rfarmer@vablet.com'
      user.name = 'Ryan Farmer(debug)'
      resolve(user)
    }
  }
)

export const getTranslations = () => new Promise(
  (resolve, reject) => {
    try {
      let formattedData = {}
      VabletNativeInterface.callNativeMethod('getCSVForFileName', {fileName: 'BaxterAssetsTranslations'}, function (response) {
        formattedData = _.keyBy(response.parsedCSV, (o) => {
          return o.BST_COUNTRY__c
        })

        resolve(formattedData)
      })
    } catch (e) {
      console.log(e)
      resolve({'DEFAULT': {BST_COUNTRY__c: 'DEFAULT', Email: 'rfarmer@vablet.com', Subject: 'Test Baxter Assets', Sentence_1: 'Test 1', Sentence_2: 'Test 2'}})
    }
  }
)
