import Vue from 'vue'
import App from './App.vue'

import i18n from '@/plugins/i18n'
import KDialogs from '@/plugins/krit-dialogs'

import Autocomplete from 'v-autocomplete'
import FlagIcon from 'vue-flag-icon'

// You need a specific loader for CSS files like https://github.com/webpack/css-loader
// import 'v-autocomplete/dist/v-autocomplete.css'

Vue.use(Autocomplete)
Vue.use(FlagIcon)
Vue.use(KDialogs)

Vue.config.productionTip = false

let loadEvent = 'DOMContentLoaded'
if (process.env.NODE_ENV === "production") {loadEvent = 'VabletLibraryLoaded'}
console.log(loadEvent)
document.addEventListener(loadEvent, function () {
// document.addEventListener('DOMContentLoaded', function () {
  new Vue({
    i18n,
    render: h => h(App)
  }).$mount('#app')
})
