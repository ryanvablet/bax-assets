export default [
  {
    "BST_COUNTRY__c": "DEFAULT",
    "Email": "rfarmer@vablet.com",
    "Subject": "Baxter Asset",
    "Sentence_1": "Asset",
    "Sentence_2": "Attached"
  },
  {
    "BST_COUNTRY__c": "Austria",
    "Email": "rfarmer@vablet.com",
    "Subject": "Test",
    "Sentence_1": "Hallo",
    "Sentence_2": ""
  },
  {
    "BST_COUNTRY__c": "Germany",
    "Email": "rfarmer@vablet.com",
    "Subject": "TEST GER",
    "Sentence_1": "Hallo",
    "Sentence_2": "Welt"
  },
  {
    "BST_COUNTRY__c": "Italy",
    "Email": "ppacun@vablet.com",
    "Subject": "Testo",
    "Sentence_1": "Ciao",
    "Sentence_2": "Mondo"
  },
  {
    "BST_COUNTRY__c": "Switzerland",
    "Email": "ppacun@vablet.com",
    "Subject": "",
    "Sentence_1": "",
    "Sentence_2": ""
  }
]