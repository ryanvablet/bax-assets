export default [
  {
    "AccountId": "0011w000003rEuRAAU",
    "SerialNumber": 12345,
    "BST_Model_ID__c": 12345,
    "InstallDate": "2018-09-20",
    "BST_Shipped_Received_Date__c": "2018-09-20",
    "Product2.Name": "123456 (TESTDL123)"
  },
  {
    "AccountId": "0011w000003rGMQAA2",
    "SerialNumber": 7890,
    "BST_Model_ID__c": 567,
    "InstallDate": "2018-09-27",
    "BST_Shipped_Received_Date__c": "2018-09-27",
    "Product2.Name": "Test_Product"
  },
  {
    "AccountId": "0011w00000742R6AAI",
    "SerialNumber": "8GN7RG2",
    "BST_Model_ID__c": "2M9811",
    "InstallDate": "2017-11-24",
    "BST_Shipped_Received_Date__c": "2017-11-24",
    "Product2.Name": "DELL SERVER PC (2M9811)"
  },
  {
    "AccountId": "0011w00000740CwAAI",
    "SerialNumber": "E16N10650",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-06-30",
    "BST_Shipped_Received_Date__c": "2017-06-30",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w0000073zvwAAA",
    "SerialNumber": "3ZGJ2J2",
    "BST_Model_ID__c": "2M9811",
    "InstallDate": "2017-06-29",
    "BST_Shipped_Received_Date__c": "2017-06-29",
    "Product2.Name": "DELL SERVER PC (2M9811)"
  },
  {
    "AccountId": "0011w00000742R6AAI",
    "SerialNumber": "E16B20587",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-11-24",
    "BST_Shipped_Received_Date__c": "2017-11-24",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w00000744TUAAY",
    "SerialNumber": "3Z8K2J2",
    "BST_Model_ID__c": "2M9811",
    "InstallDate": "2017-09-05",
    "BST_Shipped_Received_Date__c": "2017-09-05",
    "Product2.Name": "DELL SERVER PC (2M9811)"
  },
  {
    "AccountId": "0011w00000741A6AAI",
    "SerialNumber": "E16E07713",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2016-12-29",
    "BST_Shipped_Received_Date__c": "2016-12-29",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w0000073xKBAAY",
    "SerialNumber": "R154",
    "BST_Model_ID__c": "2M9824",
    "InstallDate": "2017-10-30",
    "BST_Shipped_Received_Date__c": "2017-10-30",
    "Product2.Name": "SWAP LPU (2M9824)"
  },
  {
    "AccountId": "0011w00000744TUAAY",
    "SerialNumber": "E16N10655",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-09-05",
    "BST_Shipped_Received_Date__c": "2017-09-05",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w00000744TUAAY",
    "SerialNumber": "E16N10669",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-07-26",
    "BST_Shipped_Received_Date__c": "2017-07-26",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w0000073xKBAAY",
    "SerialNumber": "8GL6RG2",
    "BST_Model_ID__c": "2M9811",
    "InstallDate": "2017-10-30",
    "BST_Shipped_Received_Date__c": "2017-10-30",
    "Product2.Name": "DELL SERVER PC (2M9811)"
  },
  {
    "AccountId": "0011w00000744iiAAA",
    "SerialNumber": 20161200207,
    "BST_Model_ID__c": 393,
    "InstallDate": "2017-06-20",
    "BST_Shipped_Received_Date__c": "2017-06-20",
    "Product2.Name": "HANDY SCALE 15 KG (=4011703) (393)"
  },
  {
    "AccountId": "0011w0000073xKBAAY",
    "SerialNumber": "E73880G6N847405",
    "BST_Model_ID__c": "ATCPLAS",
    "InstallDate": "2017-10-30",
    "BST_Shipped_Received_Date__c": "2017-10-30",
    "Product2.Name": "LASER PRINTER (ATCPLAS)"
  },
  {
    "AccountId": "0011w00000744TUAAY",
    "SerialNumber": "E16N10664",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-09-05",
    "BST_Shipped_Received_Date__c": "2017-09-05",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w00000741mBAAQ",
    "SerialNumber": "E17B16068",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-10-27",
    "BST_Shipped_Received_Date__c": "2017-10-27",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w00000744TUAAY",
    "SerialNumber": "R153",
    "BST_Model_ID__c": "2M9824",
    "InstallDate": "2017-09-05",
    "BST_Shipped_Received_Date__c": "2017-09-05",
    "Product2.Name": "SWAP LPU (2M9824)"
  },
  {
    "AccountId": "0011w00000742R6AAI",
    "SerialNumber": "E73880G6N847547",
    "BST_Model_ID__c": "ATCPLAS",
    "InstallDate": "2017-11-24",
    "BST_Shipped_Received_Date__c": "2017-11-24",
    "Product2.Name": "LASER PRINTER (ATCPLAS)"
  },
  {
    "AccountId": "0011w0000074171AAA",
    "SerialNumber": 20170600103,
    "BST_Model_ID__c": 393,
    "InstallDate": "2017-09-22",
    "BST_Shipped_Received_Date__c": "2017-09-22",
    "Product2.Name": "HANDY SCALE 15 KG (=4011703) (393)"
  },
  {
    "AccountId": "0011w000005WlE0AAK",
    "SerialNumber": "A54894RN3334",
    "BST_Model_ID__c": "",
    "InstallDate": "2017-10-18",
    "BST_Shipped_Received_Date__c": "2017-10-17",
    "Product2.Name": "AMIA AUTOMATED PERITONEAL DIAL (5C5479)"
  },
  {
    "AccountId": "0011w000002MBhlAAG",
    "SerialNumber": "",
    "BST_Model_ID__c": "",
    "InstallDate": "",
    "BST_Shipped_Received_Date__c": "",
    "Product2.Name": ""
  },
  {
    "AccountId": "0011w000002MBhlAAG",
    "SerialNumber": "",
    "BST_Model_ID__c": "",
    "InstallDate": "2018-07-23",
    "BST_Shipped_Received_Date__c": "2017-08-28",
    "Product2.Name": "!!!THIS CODE REPLACED BY PIB8152E!!! (PIB8152E)"
  },
  {
    "AccountId": "0011w000005WlE0AAK",
    "SerialNumber": "ASBJ0989",
    "BST_Model_ID__c": "2L050301DE0001",
    "InstallDate": "2015-10-05",
    "BST_Shipped_Received_Date__c": "2015-10-05",
    "Product2.Name": "VAPOR2000 AUTOEXCLUSION GERMAN (2L050301DE0001)"
  },
  {
    "AccountId": "0011w000003rEubAAE",
    "SerialNumber": 1234567,
    "BST_Model_ID__c": 1234567,
    "InstallDate": "2017-09-13",
    "BST_Shipped_Received_Date__c": "2017-05-09",
    "Product2.Name": "123456 (TESTDL123)"
  },
  {
    "AccountId": "0011w000005WlE0AAK",
    "SerialNumber": "ASBJ0988",
    "BST_Model_ID__c": "2L050301DE0002",
    "InstallDate": "2018-10-15",
    "BST_Shipped_Received_Date__c": "2018-10-15",
    "Product2.Name": "VAPOR2000 AUTOEXCLUSION GERMAN (2L050301DE0001)"
  },
  {
    "AccountId": "0011w000003rGMQAA2",
    "SerialNumber": "",
    "BST_Model_ID__c": "",
    "InstallDate": "",
    "BST_Shipped_Received_Date__c": "",
    "Product2.Name": "Test_Product"
  },
  {
    "AccountId": "0011w000003rEuWAAU",
    "SerialNumber": 123456,
    "BST_Model_ID__c": 123456,
    "InstallDate": "2018-09-20",
    "BST_Shipped_Received_Date__c": "2018-09-20",
    "Product2.Name": "123456 (TESTDL123)"
  },
  {
    "AccountId": "0011w000003pmYjAAI",
    "SerialNumber": "test",
    "BST_Model_ID__c": "",
    "InstallDate": "",
    "BST_Shipped_Received_Date__c": "",
    "Product2.Name": "123456 (TESTDL123)"
  },
  {
    "AccountId": "0011w000003rGMQAA2",
    "SerialNumber": 99990,
    "BST_Model_ID__c": 9090,
    "InstallDate": "2018-09-27",
    "BST_Shipped_Received_Date__c": "2018-07-05",
    "Product2.Name": "Medical Stretchers & Gurneys"
  },
  {
    "AccountId": "0011w0000073xKBAAY",
    "SerialNumber": "E15N37039",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-10-30",
    "BST_Shipped_Received_Date__c": "2017-10-30",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w00000744TUAAY",
    "SerialNumber": 1333,
    "BST_Model_ID__c": "2M9817",
    "InstallDate": "2017-09-05",
    "BST_Shipped_Received_Date__c": "2017-09-05",
    "Product2.Name": "DTA STATION (2M9817)"
  },
  {
    "AccountId": "0011w0000073xKBAAY",
    "SerialNumber": "B3361059",
    "BST_Model_ID__c": "2M9850336F",
    "InstallDate": "2017-10-30",
    "BST_Shipped_Received_Date__c": "2017-10-30",
    "Product2.Name": "BODY PROUD 336 (2M9850336F)"
  },
  {
    "AccountId": "0011w00000741mBAAQ",
    "SerialNumber": "E16N10662",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-10-27",
    "BST_Shipped_Received_Date__c": "2017-10-27",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w000007408KAAQ",
    "SerialNumber": "E17D01235",
    "BST_Model_ID__c": "2M9701",
    "InstallDate": "2017-10-27",
    "BST_Shipped_Received_Date__c": "2017-10-27",
    "Product2.Name": "DATALOGIC 2D WIRELESS BARCODE (2M9701)"
  },
  {
    "AccountId": "0011w000003rGMQAA2",
    "SerialNumber": "",
    "BST_Model_ID__c": "",
    "InstallDate": "",
    "BST_Shipped_Received_Date__c": "",
    "Product2.Name": "Test_Product_Germany"
  }
]