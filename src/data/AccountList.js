export default [
  {
    "Id": "0011w0000073xKBAAY",
    "Name": "UK SH CAMPUS LUBECK",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w00000744iiAAA",
    "Name": "HOME PATIENT",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w00000744TUAAY",
    "Name": "UNIVERSITATSKLINIKUM EPPENDORF",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w00000741A3AAI",
    "Name": "PATIENTEN - HEIMVERSORGUNG",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w00000741A4AAI",
    "Name": "HOME PATIENT",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w00000741A5AAI",
    "Name": "HOME PATIENT",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w00000741A6AAI",
    "Name": "FRIEDRICH-EBERT-KRANKENHAUS",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "",
    "BillingState": ""
  },
  {
    "Id": "0011w000003rEuRAAU",
    "Name": "test account austria",
    "BST_COUNTRY__c": "Austria",
    "BillingCity": "test",
    "BillingState": "test"
  },
  {
    "Id": "0011w000003rEuWAAU",
    "Name": "test account germany",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "test",
    "BillingState": "test"
  },
  {
    "Id": "0011w000003rEubAAE",
    "Name": "test account Switzerland",
    "BST_COUNTRY__c": "Switzerland",
    "BillingCity": "test",
    "BillingState": "test"
  },
  {
    "Id": "0011w000003rEv0AAE",
    "Name": "test aus acct salerep",
    "BST_COUNTRY__c": "Austria",
    "BillingCity": "Austria",
    "BillingState": "Austria"
  },
  {
    "Id": "0011w000003rGMQAA2",
    "Name": "Test_Account",
    "BST_COUNTRY__c": "Italy",
    "BillingCity": "sdf",
    "BillingState": "sdfsdf"
  },
  {
    "Id": "0011w000003rIgdAAE",
    "Name": "Test_Account",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "zxc",
    "BillingState": "zxcz"
  },
  {
    "Id": "0011w000003rXwnAAE",
    "Name": "Account_01",
    "BST_COUNTRY__c": "Germany",
    "BillingCity": "etse",
    "BillingState": "test"
  }
]